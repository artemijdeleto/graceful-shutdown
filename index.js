const faultTolerantLogger = {
    log(level, message) {
        try {
            if (!state.logger || !state.logger[level]) {
                console[level](message);

                return;
            }

            state.logger[level](message);
        } catch (e) {
            console[level](message);
        }
    },
    info(message) {
        this.log('info', message);
    },
    error(message) {
        this.log('error', message);
    }
};

const wait = (ms) => new Promise((resolve) => {
    setTimeout(() => {
        resolve('waited');
    }, ms);
});

const state = {
    isShuttingDown: false,
    handlers: [],
    timeout: null,
    logger: null,
};

function onSignal(signal) {
    faultTolerantLogger.info(`Received ${signal} signal`);

    return run({ signal });
}

function onError(error) {
    faultTolerantLogger.error(error);

    return run({ error });
}

function handleRepeatedSignal(signal) {
    faultTolerantLogger.error(`Received repeated ${signal}, exit`);

    process.exit(1);
}

function handleRepeatedError(error) {
    faultTolerantLogger.error(error);
    faultTolerantLogger.error('Received repeated error, exit');

    process.exit(1);
}

async function runHandlers() {
    for (const handler of state.handlers) {
        await handler();
    }
}

async function run(event) {
    process.on('SIGTERM', handleRepeatedSignal);
    process.on('SIGINT', handleRepeatedSignal);
    process.on('uncaughtException', handleRepeatedError);
    process.on('unhandledRejection', handleRepeatedError);

    if (state.isShuttingDown) {
        process.exit(1);
    }

    state.isShuttingDown = true;

    try {
        const resolved = await Promise.race([
            wait(state.timeout),
            runHandlers()
        ]);

        if (resolved === 'waited') {
            faultTolerantLogger.info(`Timeout exceeded, exit`);

            process.exit(1);
        }

        if (event.error) {
            process.exit(1);
        }

        faultTolerantLogger.info('Graceful shutdown succeeded');

        process.exit(0);
    } catch (err) {
        faultTolerantLogger.error(err);

        process.exit(1);
    }
}

module.exports = {
    register(opts = {}) {
        state.timeout = opts.timeout || 10000;
        state.logger = opts.logger;

        process.once('SIGTERM', onSignal);
        process.once('SIGINT', onSignal);
        process.once('uncaughtException', onError);
        process.once('unhandledRejection', onError);
    },
    onShutdown(fn) {
        state.handlers.push(fn);
    },
    shutdown() {
        return run({
            manual: true
        });
    }
};
